<?php
session_start();

if(!isset($_SESSION["login"])){
    header("Location:login.php");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
<meta content="" name="author" />

<title>Chatting Application</title>

<link rel="icon" type="image/x-icon" href="assets/img/logo.png" />
<link rel="icon" href="assets/img/logo.png" type="image/png" sizes="16x16">
<link rel='stylesheet'
    href='assets/d33wubrfki0l68.cloudfront.net/css/478ccdc1892151837f9e7163badb055b8a1833a5/light/assets/vendor/pace/pace.css' />
<script
    src='assets/d33wubrfki0l68.cloudfront.net/js/3d1965f9e8e63c62b671967aafcad6603deec90c/light/assets/vendor/pace/pace.min.js'>
</script>
<!--vendors-->
<link rel='stylesheet' type='text/css'
    href='assets/d33wubrfki0l68.cloudfront.net/bundles/291bbeead57f19651f311362abe809b67adc3fb5.css' />
<link rel='stylesheet'
    href='assets/d33wubrfki0l68.cloudfront.net/bundles/fc681442cee6ccf717f33ccc57ebf17a4e0792e1.css' />
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600" rel="stylesheet">
<!--Material Icons-->
<link rel='stylesheet' type='text/css'
    href='assets/d33wubrfki0l68.cloudfront.net/css/548117a22d5d22545a0ab2dddf8940a2e32c04ed/default/assets/fonts/materialdesignicons/materialdesignicons.min.css' />
<!--Material Icons-->
<link rel='stylesheet' type='text/css'
    href='assets/d33wubrfki0l68.cloudfront.net/css/0940f25997c8e50e65e95510b30245d116f639f0/light/assets/fonts/feather/feather-icons.css' />
<!--Bootstrap + atmos Admin CSS-->
<link rel='stylesheet' type='text/css'
    href='assets/d33wubrfki0l68.cloudfront.net/css/16e33a95bb46f814f87079394f72ef62972bd197/light/assets/css/atmos.min.css' />
<!-- Additional library for page -->
<link rel="stylesheet" href="assets/css/style.css">
<!-- CUSTOM CSS -->


</head>

<body>

    <?php

        require 'config/replies.php';    
        // Inisialisasi variabel        
        $current_user = $_SESSION['user_id']; //Mengambil id user yang login

        $query = mysqli_query($koneksi, "SELECT * FROM users WHERE user_id='$current_user'");
        $user = mysqli_fetch_assoc($query);

        // Inisialisasi variabel
        $message_id = (isset($_GET['message_id'])) ? $_GET['message_id'] : 1; //Mengambil id message dari url        
        
        // Proses Ambil Data Message
            // Deklarasi SQL
            $msgSQL = "SELECT message_body, message_time, full_name, picture FROM messages INNER JOIN users USING(user_id) WHERE message_id='$message_id'";

            $message = read($msgSQL)[0]; //Memanggil fungsi read()                        

        // Proses Ambil Data Replies
            // Deklarasi SQL
            $rplSQL = "SELECT reply_body, reply_time, full_name, picture FROM replies INNER JOIN users USING(user_id) WHERE message_id = '$message_id'";

            $replies = read($rplSQL); //Memanggil fungsi read()

        // Proses Submit Replies
            if(isset($_POST['submit'])){

                // Inisialisasi variabel            
                $reply_body = htmlspecialchars($_POST['reply_body']);
                date_default_timezone_set('Asia/Singapore'); //Set waktu menjadi waktu singapore (GMT +8)
                $reply_time = date("Y-m-d H:i:s");             

                $sql = "INSERT INTO replies(message_id,reply_body,reply_time,user_id) VALUES('$message_id', '$reply_body', '$reply_time', '$current_user')";

                $result = insert($sql);
                if($result >= 1) {
                    echo "
                        <script>
                            alert('Berhasil memberikan balasan');
                            document.location.href='replies.php?message_id=$message_id';
                        </script>
                    ";
                } else {
                    echo "
                        <script>
                            alert('Gagal memberikan balasan');
                            document.location.href='replies.php?message_id=$message_id';
                        </script>
                    "; 
                }
            }

    ?>

    <!-- SIDEBAR -->
    <aside class="admin-sidebar">
        <div class="admin-sidebar-brand">
            <!-- begin sidebar branding-->
            <img class="admin-brand-logo" src="assets/img/logo.png" width="40" alt="atmos Logo">
            <!-- end sidebar branding-->
            <div class="ml-auto">
                <!-- sidebar pin-->
                <a href="#" class="admin-pin-sidebar btn-ghost btn btn-rounded-circle"></a>
                <!-- sidebar close for mobile device-->
                <a href="#" class="admin-close-sidebar"></a>
            </div>
        </div>
        <div class="admin-sidebar-wrapper js-scrollbar">
            <ul class="menu">
                <li class="menu-item">
                    <a href="index.php" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Beranda
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder fe fe-home "></i>
                        </span>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="editprofile.php" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Profile
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder fe fe-user "></i>
                        </span>
                    </a>
                </li>
            </ul>

        </div>
    </aside>
    <!-- SIDEBAR -->

    <main class="admin-main">
        <!--site header begins-->
        <header class="admin-header">

            <a href="#" class="sidebar-toggle" data-toggleclass="sidebar-open" data-target="body"> </a>

            <nav class=" ml-auto">
                <ul class="nav align-items-center">
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <div class="avatar avatar-sm avatar-online">
                                <img src="assets/img/users/<?= $user['picture'] ?>" alt="foto profil user" class="avatar-img rounded-circle">
                            </div>
                        </a>
                        <div class="dropdown-menu  dropdown-menu-right">                        
                            <a class="dropdown-item" href="editprofile.php"> Profile
                            </a>
                            <a class="dropdown-item" href="editpassword.php"> Ganti Password
                            </a>
                            <a class="dropdown-item" href="editprofilepicture.php">Profile Picture
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="logout.php"> Logout</a>                       
                        </div>
                    </li>

                </ul>

            </nav>
        </header>
        <!--site header ends -->
        <section class="admin-content">
            <div class="container m-t-30">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Single post-->
                        <?php
                            $msg_time = date_format(date_create($message['message_time']), 'j M Y, g:i A');  //Mengubah format tanggal 
                        ?>
                        <div class="card m-b-30">
                            <div class="card-header m-t-12">
                                <div class="media">
                                    <div class="avatar mr-3 my-auto  avatar-lg">
                                        <img src="assets/img/users/<?= $message['picture']; ?>" alt="..."
                                            class="avatar-img rounded-circle">
                                    </div>
                                    <div class="media-body m-auto">
                                        <h5 class="m-0"> <?= $message['full_name']; ?> </h5>
                                        <div class="opacity-75 text-muted">
                                            <i class="fe fe-calendar"></i>
                                            <?= $msg_time; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-raw">

                            </div>
                            <div class="card-body">
                                <p>
                                    <?= $message['message_body']; ?>
                                </p>

                                <hr>

                                <div>
                                    <div class="list-unstyled p-l-24">
                                        <!-- Melakukan perulangan foreach terhadap data yang sudah diquery -->
                                        <?php foreach($replies as $data): ?>
                                            <?php
                                                $reply_time = date_format(date_create($data['reply_time']), 'j M Y, g:i A');  //Mengubah format tanggal 
                                            ?>
                                            <div class="media m-b-12">
                                                <div class="avatar mr-3  avatar-sm">
                                                    <img src="assets/img/users/<?= $data['picture']; ?>" alt="..."
                                                        class="avatar-img rounded-circle">
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="mt-0 mb-1"> <?= $data['full_name']; ?> 
                                                        <span                                         class="text-muted ml-3 small">
                                                            <i class="fe fe-calendar"></i> 
                                                            <?= $reply_time ?> 
                                                        </span>
                                                    </h6>
                                                    <p>
                                                        <?= $data['reply_body']; ?> 
                                                    </p>
                                                </div>
                                            </div>  
                                        <?php endforeach; ?>                                  
                                    </div>
                                </div>

                                <hr>

                                <div class="row align-items-start m-b-30">
                                    <div class="col-auto p-l-40">

                                        <!-- Avatar -->
                                        <div class="avatar avatar-sm">
                                            <img src="assets/img/users/<?= $user['picture']; ?>" alt="..."
                                                class="avatar-img rounded-circle">
                                        </div>

                                    </div>
                                    <div class="col ml--2">

                                        <!-- Input -->
                                        <form method="POST">
                                            <label class="sr-only">Berikan balasan...</label>
                                            <textarea name="reply_body" class="form-control"
                                                placeholder="Berikan balasan" rows="4"></textarea>
                                            <button type="submit" name="submit" class="btn btn-primary m-t-18">
                                                Kirim Balasan
                                                <i class="mdi mdi-send"></i>
                                            </button>
                                        </form>

                                    </div>

                                </div> <!-- / .row -->

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </main>


    <script src='assets/d33wubrfki0l68.cloudfront.net/bundles/85bd871e04eb889b6141c1aba0fedfa1a2215991.js'></script>
    <!--page specific scripts for demo-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-66116118-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-66116118-3');
    </script>


</body>

<!-- Mirrored from atmos.atomui.com/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Jul 2021 07:39:53 GMT -->

</html>