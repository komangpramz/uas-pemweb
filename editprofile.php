<?php
    require 'config/koneksi.php';

    session_start();

    if(!isset($_SESSION["login"])){
        header("Location:login.php");
        exit;
    }


    // Inisialisasi variabel        
    $current_user = $_SESSION['user_id']; //Mengambil id user yang login    

    $query = mysqli_query($koneksi, "SELECT * FROM users WHERE user_id='$current_user'");
    $user = mysqli_fetch_assoc($query);

    $sql = "SELECT * FROM users WHERE user_id ='$current_user'";
    $prf = mysqli_query($koneksi,$sql);

    if($prf){
        if(mysqli_num_rows($prf)>0){    
            $profil = mysqli_fetch_assoc($prf);
    ?>

<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
    <meta content="" name="author" />

    <title>Chatting Application</title>
    <link rel="icon" type="image/x-icon" href="assets/img/logo.png" />
    <link rel="icon" href="assets/img/logo.png" type="image/png" sizes="16x16">
    <link rel='stylesheet'
        href='assets/d33wubrfki0l68.cloudfront.net/css/478ccdc1892151837f9e7163badb055b8a1833a5/light/assets/vendor/pace/pace.css' />
    <script
        src='assets/d33wubrfki0l68.cloudfront.net/js/3d1965f9e8e63c62b671967aafcad6603deec90c/light/assets/vendor/pace/pace.min.js'>
    </script>
    <!--vendors-->
    <link rel='stylesheet' type='text/css'
        href='assets/d33wubrfki0l68.cloudfront.net/bundles/291bbeead57f19651f311362abe809b67adc3fb5.css' />
    <link rel='stylesheet'
        href='assets/d33wubrfki0l68.cloudfront.net/bundles/fc681442cee6ccf717f33ccc57ebf17a4e0792e1.css' />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600" rel="stylesheet">
    <!--Material Icons-->
    <link rel='stylesheet' type='text/css'
        href='assets/d33wubrfki0l68.cloudfront.net/css/548117a22d5d22545a0ab2dddf8940a2e32c04ed/default/assets/fonts/materialdesignicons/materialdesignicons.min.css' />
    <!--Material Icons-->
    <link rel='stylesheet' type='text/css'
        href='assets/d33wubrfki0l68.cloudfront.net/css/0940f25997c8e50e65e95510b30245d116f639f0/light/assets/fonts/feather/feather-icons.css' />
    <!--Bootstrap + atmos Admin CSS-->
    <link rel='stylesheet' type='text/css'
        href='assets/d33wubrfki0l68.cloudfront.net/css/16e33a95bb46f814f87079394f72ef62972bd197/light/assets/css/atmos.min.css' />
    <!-- Additional library for page -->

</head>

<body>
    <!-- SIDEBAR -->
    <aside class="admin-sidebar">
        <div class="admin-sidebar-brand">
            <!-- begin sidebar branding-->
            <img class="admin-brand-logo" src="assets/img/logo.png" width="40" alt="atmos Logo">
            <!-- end sidebar branding-->
            <div class="ml-auto">
                <!-- sidebar pin-->
                <a href="#" class="admin-pin-sidebar btn-ghost btn btn-rounded-circle"></a>
                <!-- sidebar close for mobile device-->
                <a href="#" class="admin-close-sidebar"></a>
            </div>
        </div>
        <div class="admin-sidebar-wrapper js-scrollbar">
            <ul class="menu">
                <li class="menu-item">
                    <a href="index.php" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Beranda
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder fe fe-home "></i>
                        </span>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="editprofile.php" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Profile
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder fe fe-user "></i>
                        </span>
                    </a>
                </li>           
            </ul>

        </div>
    </aside>
    <!-- SIDEBAR -->

    <main class="admin-main">
        <!--site header begins-->
        <header class="admin-header">

            <a href="#" class="sidebar-toggle" data-toggleclass="sidebar-open" data-target="body"> </a>
            
            <nav class=" ml-auto">
                <ul class="nav align-items-center">                                        
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <div class="avatar avatar-sm avatar-online">
                            <img src="assets/img/users/<?php echo $user["picture"]?>" alt="foto profil user" class="avatar-img rounded-circle">
                            </div>
                        </a>
                        <div class="dropdown-menu  dropdown-menu-right">
                            <a class="dropdown-item" href="editprofile.php"> Profile
                            </a>
                            <a class="dropdown-item" href="editpassword.php"> Ganti Password
                            </a>
                            <a class="dropdown-item" href="editprofilepicture.php"> Profile Picture
                            </a>                          
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="logout.php"> Logout</a>
                        </div>
                    </li>

                </ul>

            </nav>
        </header>        
        <!--site header ends -->
        <section class="admin-content ">
            <div class="bg-dark bg-dots m-b-30">
                <div class="container">
                    <div class="row p-b-60 p-t-60">

                        <div class="col-lg-8 text-center mx-auto text-white p-b-30">
                            <div class="m-b-10">
                                <div class="avatar avatar-lg ">
                                    <div class="avatar-title bg-info rounded-circle mdi mdi-settings "></div>
                                </div>
                            </div>
                            <h3>Profile </h3>
                        </div>


                    </div>
                </div>
            </div>
            <section class="pull-up">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-8 mx-auto  mt-2">
                            <div class="card py-3 m-b-30">
                                <div class="card-body">
                                    <form action="config/updateprofile.php" method="post">
                                        <h3 class="">Edit Profile </h3>
                                        <p class="text-muted">
                                            Halaman untuk mengubah informasi profil anda.
                                        </p><hr>

                                        <div class="form-group">
                                            <label>Full Name</label>
                                            <input type="text" name="full_name" value="<?php echo $profil['full_name'];?>" class="form-control"required placeholder="Full Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" name="username" value="<?php echo $profil["username"]?>" class="form-control"required  placeholder="Username">
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label><br>
                                            <textarea name="address" class="form-control" placeholder="Address" rows="3"><?php echo $profil["address"]?> </textarea>
                                        </div>
                                        
                                        <br>
                                        <div class="text-center">                                               
                                            <button type="submit" name= "simpan" class="btn btn-success ">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </section>

        </section>
    </main>


    <script src='assets/d33wubrfki0l68.cloudfront.net/bundles/85bd871e04eb889b6141c1aba0fedfa1a2215991.js'></script>
    <!--page specific scripts for demo-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-66116118-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-66116118-3');
    </script>
    <script
        src='assets/d33wubrfki0l68.cloudfront.net/js/adc83b19e793491b1c6ea0fd8b46cd9f32e592fc/digital-memories.fnac.be/plugins/glightbox.min.js'>
    </script>
</body>

<!-- Mirrored from atmos.atomui.com/light/setting by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Jul 2021 07:40:43 GMT -->

</html>
<?php
            
        }
    }
?>