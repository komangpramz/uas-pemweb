<?php 
    require 'config/register.php';
    require 'config/koneksi.php';
    
    if(isset($_POST["register"])){

        if(register($_POST) > 0 ){
            echo "<script>
                alert('Berhasil Register!');
                document.location.href='login.php';
            </script>";
        } else{
            echo mysqli_error($koneksi);
        }

    }
?>
<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
<meta content="" name="author"/>

<title>Chatting Application</title>
<link rel="icon" type="image/x-icon" href="assets/img/logo.png"/>
<link rel="icon" href="assets/img/logo.png" type="image/png" sizes="16x16">
<link rel='stylesheet' href='assets/d33wubrfki0l68.cloudfront.net/css/478ccdc1892151837f9e7163badb055b8a1833a5/light/assets/vendor/pace/pace.css'/>
<script src='assets/d33wubrfki0l68.cloudfront.net/js/3d1965f9e8e63c62b671967aafcad6603deec90c/light/assets/vendor/pace/pace.min.js'></script>
<!--vendors-->
<link rel='stylesheet' type='text/css' href='assets/d33wubrfki0l68.cloudfront.net/bundles/291bbeead57f19651f311362abe809b67adc3fb5.css'/>
<link rel='stylesheet' href='assets/d33wubrfki0l68.cloudfront.net/bundles/fc681442cee6ccf717f33ccc57ebf17a4e0792e1.css'/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600" rel="stylesheet">
<!--Material Icons-->
<link rel='stylesheet' type='text/css' href='assets/d33wubrfki0l68.cloudfront.net/css/548117a22d5d22545a0ab2dddf8940a2e32c04ed/default/assets/fonts/materialdesignicons/materialdesignicons.min.css'/>
<!--Material Icons-->
<link rel='stylesheet' type='text/css' href='assets/d33wubrfki0l68.cloudfront.net/css/0940f25997c8e50e65e95510b30245d116f639f0/light/assets/fonts/feather/feather-icons.css'/>
<!--Bootstrap + atmos Admin CSS-->
<link rel='stylesheet' type='text/css' href='assets/d33wubrfki0l68.cloudfront.net/css/16e33a95bb46f814f87079394f72ef62972bd197/light/assets/css/atmos.min.css'/>
<!-- Additional library for page -->

</head>
<body class="jumbo-page">

<main class="admin-main  ">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-lg-4  bg-white">
                <div class="row align-items-center m-h-100">
                    <div class="mx-auto col-md-8">

                        <form class="needs-validation" action="" method="POST">
                            <div class="p-b-20 text-center">
                                <p>
                                    <img src="assets/img/logo.svg" width="80" alt="">

                                </p>
                                <p class="admin-brand-content">
                                    Chatting Application
                                </p>
                            </div>
                            <h3 class="text-center p-b-20 fw-400">Register</h3>

                            <div class="form-row">
                                <div class="form-group floating-label col-md-12">
                                    <label for="fullname">Full Name</label>
                                    <input type="text" name="fullname" id="fullname" required class="form-control" placeholder="Full Name">
                                </div>
                                <div class="form-group floating-label col-md-12">
                                    <label for="username">Username</label>
                                    <input type="text" name="username" id="username" required class="form-control" placeholder="Username">
                                </div>
                                <div class="form-group floating-label col-md-12">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" required class="form-control " id="password" placeholder="Password">
                                </div>
                                <div class="form-group floating-label col-md-12">
                                    <label for="password2">Confirm Password</label>
                                    <input type="password" name="password2" id="password2" required class="form-control " placeholder="Confirm Password">
                                </div>
                            </div>                            
                            <button type="submit" name="register" class="btn btn-primary btn-block btn-lg">Buat Akun</button>
                        </form>

                        <p class="text-right p-t-10">
                            <a href="login.php" class="text-underline">Sudah Mempunyai Akun?</a>
                        </p>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 d-none d-md-block bg-cover" style="background-image: url('assets/img/auth.svg');">

            </div>
        </div>
    </div>
</main>


<script src='../../d33wubrfki0l68.cloudfront.net/bundles/85bd871e04eb889b6141c1aba0fedfa1a2215991.js'></script>
<!--page specific scripts for demo-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-66116118-3"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-66116118-3'); </script>
<script src='../../d33wubrfki0l68.cloudfront.net/bundles/614583066eb42c2126e72ea7baf679d1fc8b567d.js'></script>
</body>

<!-- Mirrored from atmos.atomui.com/light/signup by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Jul 2021 07:40:42 GMT -->
</html>