<?php

    // Inisialisasi variabel untuk koneksi ke database
    $host = 'localhost';
    $username = 'root';
    $pass = '';
    $db = 'chatting';

    // Mencoba konek ke database 
    try {
        $koneksi = mysqli_connect($host, $username, $pass, $db); //Kondisi koneksi berhasil
    } catch(Exception $e) {
        die('Gagal koneksi ke database: ' .$e); //Kondisi koneksi gagal
    }

?>