<?php

    // Include file koneksi terlebih dahulu
    require 'koneksi.php';

    // Fungsi untuk query database
    function insert($sql) { //Terdapat parameter SQL
        global $koneksi; //Deklarasi global variabel 

        mysqli_query($koneksi, $sql); //Jalankan query SQL

        $result = mysqli_affected_rows($koneksi); //Membuat variabel result untuk mengetahui hasil penambahan data
        return $result; // Mengembalikan nilai variabel result
    }

    // Fungsi untuk menampilkan data dari database
    function read($sql) {
        global $koneksi; //Deklarasi global variabel 

        $result = mysqli_query($koneksi, $sql);
        $rows = [];

        while($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row; //Memasukkan data row ke dalam array
        }

        return $rows; //Return data rows
    }

?>