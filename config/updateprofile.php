<?php

 include "koneksi.php";
 session_start();

 if(!isset($_SESSION["login"])){
    header("Location:login.php");
    exit;
 }

// Inisialisasi variabel        
$current_user = $_SESSION['user_id']; //Mengambil id user yang login    

$query = mysqli_query($koneksi, "SELECT * FROM users WHERE user_id='$current_user'");
$user = mysqli_fetch_assoc($query);


 if(isset($_POST['simpan']))
    {        
        $current_user = $user['user_id'];
        $fullname = $_POST['full_name'];
        $usr = $_POST['username'];
        $address = $_POST['address'];

        $check_username = mysqli_query($koneksi, "SELECT username FROM users WHERE username = '$usr'");
        
        // Kondisi ketika user tidak mengubah username
        if($user['username'] == $usr) {
            $sql = "UPDATE users SET full_name = '$fullname', address='$address' WHERE user_id = '$current_user'";
            $results = mysqli_query($koneksi,$sql);
    
            if($results)
            { 
                    echo "<script>
                    alert('Edit Profile Sukses!');
                    document.location='../index.php';
                    </script>";
            }
            else
            {
                echo "<script>
                alert('Edit Profile Gagal!');
                document.location='../editprofile.php';
                </script>";
            }

        // Kondisi ketika user mengubah username
        } else {
            // Pengecekan username dalam database
            if(mysqli_num_rows($check_username) >= 1) {
                echo "<script>
                    alert('Username telah digunakan!');
                    document.location='../editprofile.php';
                    </script>";
            } else {
                $sql = "UPDATE users SET full_name = '$fullname', username ='$usr', address='$address' WHERE user_id = '$current_user'";
                $results = mysqli_query($koneksi,$sql);
        
                if($results)
                { 
                        echo "<script>
                        alert('Edit Profile Sukses!');
                        document.location='../index.php';
                        </script>";
                }
                else
                {
                    echo "<script>
                    alert('Edit Profile Gagal!');
                    document.location='../editprofile.php';
                    </script>";
                }
            } 
        }               
        
    }
?>