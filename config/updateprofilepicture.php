<?php

 include "koneksi.php";
 session_start();

 if(!isset($_SESSION["login"])){
     header("Location:login.php");
     exit;
 }

 // Inisialisasi variabel        
 $current_user = $_SESSION['user_id']; //Mengambil id user yang login    

 $query = mysqli_query($koneksi, "SELECT * FROM users WHERE user_id='$current_user'");
 $user = mysqli_fetch_assoc($query);

 if(isset($_POST['simpan'])){        

        $picture=$_FILES['picture']['name'];
        $extension = pathinfo($picture, PATHINFO_EXTENSION);      
        $path=$_FILES['picture']['tmp_name'];

        if($picture != null){
            // Pengecekan ekstensi file yang diupload
            if($extension != 'jpg' && $extension != 'png' && $extension != 'jpeg' && $extension != 'JPG' && $extension != 'PNG' && $extension != 'JPEG'){            
                echo "<script>
                    alert('Extensi file harus JPG/JPEG/PNG!');
                    document.location='../editprofilepicture.php';
                    </script>";
                exit;
            } else {
                $store_foto = $user['user_id']."-".rand(1,100)."-".$picture; //Mengubah nama foto sesuai format
                move_uploaded_file($path, "../assets/img/users/".$store_foto);

                $sql = "UPDATE users SET picture = '$store_foto' WHERE user_id = '$current_user'";
                $results = mysqli_query($koneksi,$sql);

                if($results)
                { 
                        echo "<script>
                        alert('Edit Profile Picture Sukses!');
                        document.location='../index.php';
                        </script>";
                }
                else
                {
                    echo "<script>
                    alert('Edit Profile Picture Gagal!');
                    document.location='../editprofilepicture.php';
                    </script>";
                }
            }  
        } else {
            header("Location: ../index.php");
        }
              
        
    }
?>